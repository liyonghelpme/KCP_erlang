%% kcppacket

-module(kcppacket).
-author('liyong').
-export([encodeFull/1, decodeData/1]).
-include("kcppack.hrl").




encodeFull(Packet) ->
	case Packet#kcppack.isAck of
		1 -> 
		Packet#kcppack{
			fulldata = [Packet#kcppack.isAck | intToBin(Packet#kcppack.sn)]
		};
		0 -> 
		Packet#kcppack{
			fulldata = [Packet#kcppack.isAck | intToBin(Packet#kcppack.sn) ] ++ Packet#kcppack.data 
		}
	end.
	
decodeData(Packet) ->
	Packet#kcppack{
		data = element(2, lists:split(5, Packet#kcppack.fulldata))
	}.


intToBin(IntValue) ->
	Iv1 = IntValue rem 256,
	Dv1 = IntValue div 256, 

	Iv2 = Dv1 rem 256,
	Dv2 = Dv1 div 256, 

	Iv3 = Dv2 rem 256,
	Dv3 = Dv2 div 256, 

	Iv4 = Dv3 rem 256,
	% Dv4 = Dv3 div 256, 

	[Iv1, Iv2, Iv3, Iv4].
